package az.ingress.hellospringboot.controller;

import az.ingress.hellospringboot.dto.SearchCriteria;
import az.ingress.hellospringboot.dto.Spesification;
import az.ingress.hellospringboot.dto.StudentDto;
import az.ingress.hellospringboot.req.StudentReq;
import az.ingress.hellospringboot.service.StudentService;
import com.speedment.jpastreamer.application.JPAStreamer;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/student")
@Slf4j
public class StudentController {

    private final StudentService studentService;

    @GetMapping("")
    public List<StudentDto> getAllStudents(){
        try{
            return studentService.getAllStudents();

        }catch (Exception e){
            log.info("exeception",e);
        }
        return studentService.getAllStudents();
    }

    @GetMapping("/{id}")
    public StudentDto getStudentById(@PathVariable(value = "id") Integer id){
        try{
            log.info("id "+id);
        return studentService.getStudentById(id);
        }catch (Exception e){
            log.info("exeception",e);
        }
        return studentService.getStudentById(id);

    }

    @PutMapping("/{id}")
    public StudentDto updateStudent(@PathVariable(value = "id") Integer id, @RequestBody StudentReq req){
        try{
        return studentService.updateStudent(id,req);
        }catch (Exception e){
            log.info("exeception",e);
        }
        return studentService.updateStudent(id,req);

    }

    @PostMapping("")
    public StudentDto saveStudent(@RequestBody StudentReq req){
        try{
        return studentService.saveStudent(req);
        }catch (Exception e){
            log.info("exeception",e);
        }
        return studentService.saveStudent(req);

    }




    @GetMapping("/jpa")
    public List<StudentDto> getAllStudentsWithJpaStreamer(){
        try{
            return studentService.getAllStudentsWithJpaStreamer();

        }catch (Exception e){
            log.info("exeception",e);
        }
        return studentService.getAllStudentsWithJpaStreamer();
    }

    @GetMapping("/jpa/{id}")
    public StudentDto getStudentByIdWithJpaStreamer(@PathVariable(value = "id") Integer id){
        try{
            log.info("id "+id);
            return studentService.getStudentByIdWithJpaStreamer(id);
        }catch (Exception e){
            log.info("exeception",e);
        }
        return studentService.getStudentByIdWithJpaStreamer(id);

    }

    @PutMapping("/jpa/{id}")
    public StudentDto updateStudentWithJpaStreamer(@PathVariable(value = "id") Integer id, @RequestBody StudentReq req){
        try{
            return studentService.updateStudentWithJpaStreamer(id,req);
        }catch (Exception e){
            log.info("exeception",e);
        }
        return studentService.updateStudentWithJpaStreamer(id,req);

    }

    @GetMapping("/filter")
    public List<StudentDto> getAllStudentsWithCriteriaBuilder(@RequestBody List<SearchCriteria> criteria){
        System.out.println("geldii");

        try{
            System.out.println("geldii");
            log.info("geldi");
            Spesification a = new Spesification();
            a.add(criteria);
            return studentService.getAllStudentsWithCriteriaBuilder(a);

        }catch (Exception e){
            e.printStackTrace();
            log.info("exeception",e);
        }
        return null;
    }

}
