package az.ingress.hellospringboot.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class MainController {

    @GetMapping("/{name}")
    public String HelloUser(@PathVariable(value = "name") String  name){
        return "Hello "+name;
    }


}
