package az.ingress.hellospringboot.controller;

import az.ingress.hellospringboot.dto.StudentDto;
import az.ingress.hellospringboot.entity.Account;
import az.ingress.hellospringboot.service.AccountService;
import az.ingress.hellospringboot.service.StudentService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/account")
@Slf4j
public class AccountController {
    private final AccountService accountService;

    @GetMapping("/{id}")
    public Account getAccountById(@PathVariable(value = "id") Long id){
            log.info("id "+id);
            return accountService.getAccountById(id);

    }

}
