package az.ingress.hellospringboot;

import az.ingress.hellospringboot.repository.AccountRepository;
import az.ingress.hellospringboot.repository.StudentRepo;
import az.ingress.hellospringboot.service.TransferService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cache.annotation.EnableCaching;

import javax.persistence.EntityManagerFactory;

@SpringBootApplication
@RequiredArgsConstructor
@Slf4j
@EnableConfigurationProperties
//@EnableCaching
public class HellospringbootApplication implements CommandLineRunner {

    private final StudentRepo studentRepo;
    private final AccountRepository accountRepository ;
    private final EntityManagerFactory entityManagerFactory;
    private final TransferService transferService;

    public static void main(String[] args) {
        SpringApplication.run(HellospringbootApplication.class, args);

    }


    @Override
    public void run(String... args) throws Exception {
//            accountRepository.save(new Account(1L,"Fuad",200.0));
//            accountRepository.save(new Account(2L,"Murad",200.0));

//            Account from = accountRepository.findById(3L).get();
//            Account to = accountRepository.findById(4L).get();
//            transferService.transfer(from,to,50.0);
    }
}
