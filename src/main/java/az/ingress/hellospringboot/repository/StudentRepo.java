package az.ingress.hellospringboot.repository;

import az.ingress.hellospringboot.entity.Student;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface StudentRepo extends CrudRepository<Student,Integer>, JpaSpecificationExecutor<Student> {
    @Override
    List<Student> findAll();

}
