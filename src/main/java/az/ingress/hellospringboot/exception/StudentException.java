package az.ingress.hellospringboot.exception;


import az.ingress.hellospringboot.enums.ResultCode;

public class StudentException extends BaseException {


    public StudentException(ResultCode result) {
        super(result);
    }

    public static StudentException studentNotFound() {
        return new StudentException(ResultCode.STUDENT_NOT_FOUND);
    }
    public static StudentException dateValueNotCorrect() {
        return new StudentException(ResultCode.DATE_VALUE_NOT_CORRECT);
    }

}
