
package az.ingress.hellospringboot.exception;

import az.ingress.hellospringboot.enums.ResultCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public class BaseException extends RuntimeException {
    private final ResultCode result;
}
