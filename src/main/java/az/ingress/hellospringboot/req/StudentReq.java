package az.ingress.hellospringboot.req;

import lombok.Data;

@Data
public class StudentReq {
    private String name;
    private String institute;
    private String birthDate;
    private String address;
    private String phoneNumber;
    private String phoneNumber2;


}
