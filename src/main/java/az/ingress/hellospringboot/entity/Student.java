package az.ingress.hellospringboot.entity;

import lombok.*;
import lombok.experimental.FieldDefaults;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import  org.hibernate.annotations.Cache;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
@Cache(region = "studentCache", usage = CacheConcurrencyStrategy.READ_WRITE)
public class Student {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", insertable = false)
    Long id;
    String firstName;
    String lastName;
    String institute;
    LocalDateTime birthdate;
    Double totalGpa;

    @OneToOne(cascade = CascadeType.ALL)
    Address address;

//    @OneToMany(mappedBy = "student",cascade = CascadeType.ALL)
//    List<PhoneNumber> phoneNumberList;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "student_phone",
            joinColumns =  @JoinColumn(name = "phone_number_id",referencedColumnName = "id"),
            inverseJoinColumns =@JoinColumn(name = "student_id",referencedColumnName = "id"))
    List<PhoneNumber> phoneNumberList;

    @ManyToMany
    @JoinTable(name = "student_role",
    joinColumns = @JoinColumn(name = "student_id",referencedColumnName = "id"),
    inverseJoinColumns = @JoinColumn(name = "role_id",referencedColumnName = "id"))
    List<Role> roles;


}
