package az.ingress.hellospringboot.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum ResultCode {
    OK(1, "success"),
    STUDENT_NOT_FOUND(-1, "student not found"),
    DATE_VALUE_NOT_CORRECT(-1,"date value is not Correct please use yyyy-MM-dd this format"),


    URL_NOT_FOUND(-1, "url not found"),

    INVALID_FIELD_VALUE(-3, "invalid field value"),
    INVALID_CREDENTIALS(-3, "authentication required"),


    REQUEST_METHOD_NOT_SUPPORTED(-9, "request method not supported"),
    SYSTEM_ERROR(-10, "system error");

    private final int code;
    private final String value;


}
