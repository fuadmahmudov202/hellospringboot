package az.ingress.hellospringboot.service;

import az.ingress.hellospringboot.dto.SearchCriteria;
import az.ingress.hellospringboot.dto.Spesification;
import az.ingress.hellospringboot.dto.StudentDto;
import az.ingress.hellospringboot.req.StudentReq;

import java.util.List;

public interface StudentService {
    List<StudentDto> getAllStudents();

    StudentDto getStudentById(Integer id);

    StudentDto updateStudent(Integer id, StudentReq req);

    StudentDto saveStudent(StudentReq req);

    List<StudentDto> getAllStudentsWithJpaStreamer();

    StudentDto getStudentByIdWithJpaStreamer(Integer id);

    StudentDto updateStudentWithJpaStreamer(Integer id, StudentReq req);

    List<StudentDto> getAllStudentsWithCriteriaBuilder(Spesification spesification);
}
