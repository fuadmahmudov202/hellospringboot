package az.ingress.hellospringboot.service;

import az.ingress.hellospringboot.entity.Account;

public interface TransferService {

    void transfer(Account from, Account to, Double amount);
}
