package az.ingress.hellospringboot.service.impl;

import az.ingress.hellospringboot.entity.Account;
import az.ingress.hellospringboot.repository.AccountRepository;
import az.ingress.hellospringboot.service.AccountService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class AccountServiceImpl implements AccountService {
    private AccountRepository accountRepository;
    @Override
    public Account getAccountById(Long id) {
        try {
            log.info(accountRepository.findById(id).toString());
            return accountRepository.findById(id).get();
        }catch (Exception e){
            e.printStackTrace();
            log.info("excep",e);
        }
        return accountRepository.findById(id).get();
    }
}
