package az.ingress.hellospringboot.service.impl;

import az.ingress.hellospringboot.entity.Account;
import az.ingress.hellospringboot.repository.AccountRepository;
import az.ingress.hellospringboot.service.TransferService;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@RequiredArgsConstructor
@Slf4j
public class TransferServiceİmpl implements TransferService {
    private final AccountRepository accountRepository;

    @Transactional
    @SneakyThrows
    public void transfer(Account from, Account to, Double amount){
        log.info("trying to transfer money");
        if (from.getBalance()<amount){
            throw new RuntimeException("Not sufficient Balance");
        }
        log.info("waiting 5 second");
        Thread.sleep(10_000);

        from.setBalance(from.getBalance()-amount);
        to.setBalance(to.getBalance()+amount);

        accountRepository.save(from);
        accountRepository.save(to);

    }
}
