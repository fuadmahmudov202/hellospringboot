package az.ingress.hellospringboot.service.impl;

import az.ingress.hellospringboot.dto.*;
import az.ingress.hellospringboot.entity.Address;
import az.ingress.hellospringboot.entity.PhoneNumber;
import az.ingress.hellospringboot.entity.Student;
import az.ingress.hellospringboot.exception.StudentException;
import az.ingress.hellospringboot.repository.StudentRepo;
import az.ingress.hellospringboot.req.StudentReq;
import az.ingress.hellospringboot.service.StudentService;
import com.speedment.jpastreamer.application.JPAStreamer;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class StudentServiceImpl implements StudentService {

    private final StudentRepo studentRepo;
    private final JPAStreamer jpaStreamer;

    @Override
    public List<StudentDto> getAllStudents() {
        return studentRepo.findAll()
                .stream()
                .map(student -> mapToDto(student))
                .collect(Collectors.toList());
    }

    private StudentDto mapToDto(Student s) {
//        log.info("log"+s);
        //TODO
        //xeta atir log
        return StudentDto.builder()
                .id(s.getId())
                .birthdate(s.getBirthdate())
                .institute(s.getInstitute())
                .name(s.getFirstName())
//                .address(AddressDto.builder()
//                        .id(s.getAddress().getId())
//                        .name(s.getAddress().getName()).build()
//                )
                .phoneNumberList(s.getPhoneNumberList().stream().map(this::mapPhoneToDto).collect(Collectors.toList()))
                .build();
    }

    private PhoneNumberDto mapPhoneToDto(PhoneNumber phoneNumber) {
        return PhoneNumberDto.builder().id(phoneNumber.getId())
                .name(phoneNumber.getPhone()).build();
    }

    @Override
    public StudentDto getStudentById(Integer id) {
         Student student=studentRepo.findById(id)
                 .orElseThrow(StudentException::studentNotFound);
         return mapToDto(student);
    }

    @Override
    public StudentDto updateStudent(Integer id, StudentReq req) {
        Student student=studentRepo.findById(id)
                .orElseThrow(StudentException::studentNotFound);
        if (req.getName()!=null)
            student.setFirstName(req.getName());
        if (req.getInstitute()!=null)
            student.setInstitute(req.getInstitute());
        if (req.getBirthDate()!=null)
            student.setBirthdate(convertStringToLocalDate(req.getBirthDate()));

        return mapToDto(studentRepo.save(student));
    }

    @Override
    public StudentDto saveStudent(StudentReq req) {

        List<PhoneNumber>phoneNumberList=new ArrayList<>();
        Student student=Student.builder()
                .birthdate(convertStringToLocalDate(req.getBirthDate()))
                .institute(req.getInstitute())
                .firstName(req.getName())
                .address(Address.builder().name(req.getAddress()).build())
                .build();
        phoneNumberList.add(PhoneNumber.builder().phone(req.getPhoneNumber()).student(student).build());
        phoneNumberList.add(PhoneNumber.builder().phone(req.getPhoneNumber2()).student(student).build());

        student.setPhoneNumberList(phoneNumberList);
        log.info("body"+req.toString());
        log.info("a");

//        try {
//            log.info("student"+student.getPhoneNumberList());
//        }catch (Exception e) {
//            log.info("a",e);
//        }
        return mapToDto(studentRepo.save(student));
    }
    public LocalDateTime convertStringToLocalDate(String date){
        LocalDateTime d= null;
        try{
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
             d = LocalDateTime.parse(date, formatter);
        }catch (Exception e){
            StudentException.dateValueNotCorrect();
        }
        return d;
    }





    @Override
    public List<StudentDto> getAllStudentsWithJpaStreamer() {
        return jpaStreamer.stream(Student.class).map(student -> mapToDto(student))
                .collect(Collectors.toList());
    }
    @Override
    public StudentDto getStudentByIdWithJpaStreamer(Integer id) {
        return jpaStreamer.stream(Student.class)
                .filter(student1 -> student1.getId().intValue()==id)
                .findAny().map(this::mapToDto)
                .orElseThrow(StudentException::studentNotFound);
    }

    @Override
    public StudentDto updateStudentWithJpaStreamer(Integer id, StudentReq req) {
        Student student=jpaStreamer.stream(Student.class)
                .filter(student1 -> student1.getId().intValue()==id)
                .findAny()
                .orElseThrow(StudentException::studentNotFound);
        if (req.getName()!=null)
            student.setFirstName(req.getName());
        if (req.getInstitute()!=null)
            student.setInstitute(req.getInstitute());
        if (req.getBirthDate()!=null)
            student.setBirthdate(convertStringToLocalDate(req.getBirthDate()));

        return mapToDto(studentRepo.save(student));
    }

    @Override
    public List<StudentDto> getAllStudentsWithCriteriaBuilder(Spesification criteria) {
        return studentRepo.findAll(criteria).stream()
                .map(this::mapToDto)
                .collect(Collectors.toList());
    }
}
