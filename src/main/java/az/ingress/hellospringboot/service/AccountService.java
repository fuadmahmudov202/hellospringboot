package az.ingress.hellospringboot.service;

import az.ingress.hellospringboot.entity.Account;

public interface AccountService {
    Account getAccountById(Long id);
}
