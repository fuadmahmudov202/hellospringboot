FROM alpine:3.11.2
RUN apk add --no-cache openjdk11
COPY build/libs/hellospringboot-0.0.1-SNAPSHOT.jar /app/app.war
WORKDIR /app
ENTRYPOINT ["java"]
CMD ["-jar","app.war"]
